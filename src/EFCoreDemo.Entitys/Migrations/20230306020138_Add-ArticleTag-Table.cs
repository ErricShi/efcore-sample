﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreDemo.Entitys.Migrations
{
    public partial class AddArticleTagTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<string>(
            //    name: "ArticleEntityId",
            //    table: "Tags",
            //    type: "nvarchar(50)",
            //    nullable: true);

            migrationBuilder.CreateTable(
                name: "ArticleTags",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ArticleId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    TagId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDel = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleTags", x => x.Id);
                    //table.ForeignKey(
                    //    name: "FK_ArticleTags_Articles_ArticleId",
                    //    column: x => x.ArticleId,
                    //    principalTable: "Articles",
                    //    principalColumn: "Id",
                    //    onDelete: ReferentialAction.Restrict);
                    //table.ForeignKey(
                    //    name: "FK_ArticleTags_Tags_TagId",
                    //    column: x => x.TagId,
                    //    principalTable: "Tags",
                    //    principalColumn: "Id",
                    //    onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tags_ArticleEntityId",
            //    table: "Tags",
            //    column: "ArticleEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleTags_ArticleId",
                table: "ArticleTags",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleTags_TagId",
                table: "ArticleTags",
                column: "TagId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Tags_Articles_ArticleEntityId",
            //    table: "Tags",
            //    column: "ArticleEntityId",
            //    principalTable: "Articles",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tags_Articles_ArticleEntityId",
                table: "Tags");

            migrationBuilder.DropTable(
                name: "ArticleTags");

            migrationBuilder.DropIndex(
                name: "IX_Tags_ArticleEntityId",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "ArticleEntityId",
                table: "Tags");
        }
    }
}
