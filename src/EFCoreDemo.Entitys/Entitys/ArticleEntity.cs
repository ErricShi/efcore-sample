﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    /// <summary>
    /// 文章类型
    /// </summary>
    public class ArticleEntity : BaseEntity
    {
        /// <summary>
        /// 用户id
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// 文章标题
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// 概要
        /// </summary>
        [MaxLength(500)]
        public string Summary { get; set; }

        /// <summary>
        /// 封面图
        /// </summary>
        [MaxLength(500)]
        public string Poster { get; set; }
    }
}
