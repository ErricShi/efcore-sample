﻿using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    public class UserEntity : BaseEntity
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Gender { get; set; } = 1;
    }
}
