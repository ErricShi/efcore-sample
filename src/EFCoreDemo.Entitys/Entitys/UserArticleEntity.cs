﻿using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    /// <summary>
    /// 用户文章
    /// </summary>
    public class UserArticleEntity : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// 文章ID
        /// </summary>
        [MaxLength(50)]
        public string ArticleId { get; set; }
    }
}
