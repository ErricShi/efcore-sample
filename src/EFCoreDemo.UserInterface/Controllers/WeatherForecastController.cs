﻿using EFCoreDemo.Entitys.Entitys;
using EFCoreDemo.IService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EFCoreDemo.UserInterface.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly IArticleService _articleService;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IArticleService articleService)
        {
            _logger = logger;
            _articleService = articleService;
        }

        [HttpPost("AddArticle")]
        public async Task<object> AddArticle()
        {  
            return await _articleService.AddArticle();
        }
        [HttpGet("GetArticles")]
        public async Task<object> GetArticles()
        { 
            return await _articleService.GetArticles();
        }
        [HttpGet("GetArticleInclude")]
        public async Task<object> GetArticleInclude()
        {
            return await _articleService.GetArticle("01b653c4b47e4e198aeee74c5a4f6dff"); 
        }

        [HttpGet("GetArticle")]
        public async Task<object> GetArticle()
        {
            //return await _articleService.GetArticle("d3db68710eb8448fa2ca0f6d2229b07e");
            return await _articleService.GetArticlesInnerJoin();
        }

        [HttpGet("GetArticle-LeftJoin")]
        public async Task<object> GetArticleLeftJoin()
        => await _articleService.GetArticlesLeftJoin();



        [HttpGet]
        public async Task<object> Get()
        {
            var article = await _articleService.GetArticle("ca488fdc13614575b2069f7549170047");
            //await _articleService.BatchDeleteAsync(m => m.CreateTime > Convert.ToDateTime("2023-03-02"));
            //await _articleService.AddArticle();
            //await _articleService.InsertAsync(new ArticleEntity
            //{
            //    Id = Guid.NewGuid().ToString(),
            //    Poster = "http://www.baidu.com",
            //    Summary = "新梗概，新总结，新概要",
            //    Title = "新标题"
            //});
            await _articleService.BatchUpdateAsync(
                m => m.CreateTime <= Convert.ToDateTime("2023-03-02"),
                m => new ArticleEntity { Poster = "http;//www.taobao.com" });

            var articles = await _articleService.Select(1, 10,
                m => m.CreateTime <= Convert.ToDateTime("2023-03-02"),
                m => m.CreateTime, false);
            return articles;
        }
    }
}
