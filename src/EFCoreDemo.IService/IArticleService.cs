﻿using EFCoreDemo.Entitys.Entitys;
using System.Threading.Tasks;

namespace EFCoreDemo.IService
{
    public interface IArticleService : IBaseService<ArticleEntity>
    {
        Task<bool> AddArticle();

        Task<ArticleEntity> GetArticle(string id);

        Task<object> GetArticles();

        Task<object> GetArticlesInnerJoin();

        Task<object> GetArticlesLeftJoin();

        Task<object> GetArticlesLeftJoins();
    }
}
