﻿using EFCoreDemo.Entitys.Entitys;
using EFCoreDemo.IRepositorys;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Repositorys
{
    public class TagRepository : BaseRepository<TagEntity>, ITagRepository
    {
        public TagRepository(DbContext dbContext) : base(dbContext) { }
    }
}
