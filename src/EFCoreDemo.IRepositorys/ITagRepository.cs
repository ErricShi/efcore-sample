﻿using EFCoreDemo.Entitys.Entitys;

namespace EFCoreDemo.IRepositorys
{
    public interface ITagRepository : IBaseRepository<TagEntity>
    {
    }
}
